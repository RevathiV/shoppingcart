'use strict';
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      await queryInterface.bulkInsert('Groups', [{
        name: 'mens wear',
        description:"Men's clothes are articles of clothing designed for and worn by men",
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'womens wear',
        description:"Women's clothes are articles of clothing designed for and worn by women",
        isactive:'true',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {name: 'kids wear',
      description:"kid's clothes are articles of clothing designed for and worn by kids",
      isactive:'false',
      createdAt: new Date(),
      updatedAt: new Date(),}], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Groups', null, {});
  }
};
