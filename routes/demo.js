var express = require('express');
var router = express.Router();
var groupscontroller = require('../controller/groupscontroller');
const groups = require('../models/groups');

router.get('/', groupscontroller.getAll);
router.get('/withcategories',groupscontroller.getAllWithCategories);


module.exports = router;