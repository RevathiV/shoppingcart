var Model = require('../models');
module.exports.getAll = function (req, res) {
  Model.Categories.findAll()
    .then(function (groups) {
      res.send(groups);
    })
}

 module.exports.getAllWithCategories = function (req, res) {
  Model.Categories.findAll()
    .then(function (categories) {
      res.send(categories);
    })
}