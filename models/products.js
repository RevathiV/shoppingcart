'use strict';
const {
  Model
} = require('sequelize');
const categories = require('./categories');
module.exports = (sequelize, DataTypes) => {
  class Products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
   
  }
  Products.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    image_url: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    isactive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Products',
  });
  Products.associate=function(models){
    Products.belongsTo(models.Categories);
  }
  return Products;
};